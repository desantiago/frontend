import { createContext, useContext, useState } from "react";

export type CartState = {
    cartOpen?: boolean,
    openCart?: () => void,
    closeCart?: () => void,

    colorSelected?: string,
    sizeSelected?: string,
    setColorSelected?: (string) => void,
    setSizeSelected?: (string) => void
}

const LocalStateContext = createContext<CartState>({});
const LocalStateProvider = LocalStateContext.Provider;

function CartStateProvider({ children }) {
    const [cartOpen, setCartOpen] = useState(false);
    const [colorSelected, setColorSelected] = useState('');
    const [sizeSelected, setSizeSelected] = useState('');

    const openCart = () => {
        setCartOpen(true);
    }

    const closeCart = () => {
        setCartOpen(false);
    }

    const toogleCart = () => {
        setCartOpen(!cartOpen);
    }

    const values: CartState = {
        cartOpen,
        colorSelected,
        sizeSelected,
        openCart,
        closeCart,
        setColorSelected,
        setSizeSelected
    }

    return (
        <LocalStateProvider value={values}>{children}</LocalStateProvider>
    );
}

function useCart(): CartState {
    const all = useContext(LocalStateContext);
    return all;
}

export { CartStateProvider, useCart }