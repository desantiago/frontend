
import { File, Sizes, Color, Product, Image as TypeImage } from '../lib/types';

const emptyColor: Color = {
    key: '',
    name: '',
    hex: '',
    id: ''
}
const emptyFile: File = {
    key: '',
    filename: '',
    mimetype: '',
    encoding: ''
}
const emptySizes: Sizes = {
    color: null,
    sizes: []
}

const emptyProduct: Product = {
    id: '',
    key: '',
    slug: '',
    name: '',
    description: '',
    sizeAndFit: '',
    startSize: 0,
    endSize: 0,
    price: 0,
    details: '',
    category: {
        id: '',
        key: '',
        description: '',
        slug: '',
        timeStamp: '',
        subcategories: []
    },
    subCategory: {
        id: '',
        key: '',
        description: '',
        slug: '',
        timeStamp: '',
        categoryKey: '',
        category: {
            id: '',
            key: '',
            description: '',
            slug: '',
            timeStamp: '',
            subcategories: []
        }
    },
    brand: {
        id: '',
        key: '',
        description: '',
        slug: '',
        timeStamp: '',
    },

    colors: [],
    images: [],
    relatedProducts: [],
    thumbnail: {
        color: {
            id: '',
            key: '',
            name: '',
            hex: ''
        },
        image: {}
    },
    mainImage: {
        color: {
            id: '',
            key: '',
            name: '',
            hex: ''
        },
        image: {}
    },
};

export {
    emptyColor,
    emptyFile,
    emptySizes,
    emptyProduct,
}

