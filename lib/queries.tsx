import { gql } from "@apollo/client";

export const PRODUCT_QUERY = gql`
query PRODUCT_QUERY($id: String) {
    product: productByIds(id: $id) {
        id
        name
        description
        sizeAndFit
        details
        category {
            key
            description
        }
        subCategory {
            key
            description
        }
        brand {
            key
            description
        }
        colors {
            id
            key
            name
        }
        images {
            color {
                id
                key
                name
            }
            images {
                file {
                    id
                    filename
                    public_id
                }
                order
            }
        }
        sizes {
            color {
                id
                key
                name
            }
            sizes
        }
        prices {
            color {
                id
                key
                name
            }
            prices {
                size
                price
            }
        }
        relatedProducts {
            order
            product {
                id
                name
                slug
                brand {
                    description
                }
                prices {
                    prices {
                        size
                        price
                    }
                }        
                thumbnail {
                    image {
                        public_id
                    }
                }
                mainImage {
                    image {
                        public_id
                    }
                }    
            }
        }
    }
}
`
export const ALL_PRODUCTS_QUERY = gql`
    query ALL_PRODUCTS_QUERY($where: WhereInput, $skip: Int, $first: Int) {
        products: productsByParams(where: $where, skip: $skip, first: $first) {
            id
            key
            name
            slug
            brand {
                slug
                description
            }
            prices {
                prices {
                    size
                    price
                }
            }
            thumbnail {
                image {
                    public_id
                }
            }
            mainImage {
                image {
                    public_id
                }
            }
        }
        aggregations: aggregationsByParams(where: $where) {
            count
            mode
            colors {
                id
                key
                name
            }
            sizes
            brands {
                slug
                description
            }
            categories {
                category {
                    slug
                    description
                }
                subCategories {
                    slug
                    description
                }
            }
        }
        aggregationsCategory: aggregationsNoFilters(where: $where) {
            count
            mode
            colors {
                id
                key
                name
            }
            sizes
            brands {
                slug
                description
            }
            categories {
                category {
                    slug
                    description
                }
                subCategories {
                    slug
                    description
                }
            }
        }
    }
`
