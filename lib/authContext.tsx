import React, { useState } from "react";
import PropTypes from 'prop-types';

const AuthContext = React.createContext({});


export const AuthProvider = props => {
    const [token, setToken] = useState('');
    const values = {
        token,
        setToken
    }

    return (
        <AuthContext.Provider value={values}> { props.children} </AuthContext.Provider>
    )
}

export const AuthConsumer = AuthContext.Consumer;
export default AuthContext;

AuthProvider.propTypes = {
    children: PropTypes.node.isRequired,
};