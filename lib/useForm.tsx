import { useState } from "react";

export interface FormFields {
    email: string,
    password: string,
    name?: string
}
export function useForm(initial: FormFields = {
    email: '',
    password: ''
}) {
    const [inputs, setInputs] = useState(initial);

    function handleChange(e): void {
        let { value, name, type } = e.target;
        if (type === 'number') {
            value = Number(value);
        }
        if (type === 'file') {
            [value] = e.target.files;
        }

        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        });
    }

    return {
        inputs,
        handleChange,
    }
}