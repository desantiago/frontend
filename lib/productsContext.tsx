import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { gql, useLazyQuery, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import { perPage } from '../config';
// import { useCategories } from './categoriesProvider';
import { BRAND, CATEGORY, SUBCATEGORY } from './types';
import { ALL_PRODUCTS_QUERY } from './queries';
import { QueryParams, Aggregation } from './types';

// const ALL_CATEGORIES_QUERY = gql`
//     query ALL_CATEGORIES_QUERY {
//         categories {
//             key
//             slug
//             description  
//             subcategories {
//                 slug
//                 key
//                 description
//             }
//         }
//     }
// `
// aggregations(category: $category, subCategory: $subCategory, brands: $brands, colors: $colors, sizes: $sizes) {

// const AGGREGATIONS_CATEGORY_QUERY = gql`
//     query AGGREGATIONS_CATEGORY_QUERY($category: [String], $subCategory: [String], $brands: [String],) {
//         aggregations(category: $category, subCategory: $subCategory, brands: $brands) {
//             count
//             colors {
//                 id
//                 key
//                 name
//             }
//             sizes
//             brands {
//                 slug
//                 description
//             }
//             categories {
//                 category {
//                     slug
//                     description
//                 }
//                 subCategories {
//                     slug
//                     description
//                 }
//             }
//         }
//     }
// `

// const ALL_PRODUCTS_QUERY = gql`
//     query ALL_PRODUCTS_QUERY($category: [String], $subCategory: [String], $brands: [String], $colors: [String], $sizes: [String], $skip: Int, $first: Int) {
//         products(category: $category, subCategory: $subCategory, brands: $brands, colors: $colors, sizes: $sizes, skip: $skip, first: $first) {
//             id
//             key
//             name
//             brand {
//                 slug
//                 description
//             }
//             prices {
//                 prices {
//                     size
//                     price
//                 }
//             }
//             thumbnail {
//                 image {
//                     public_id
//                 }
//             }
//             mainImage {
//                 image {
//                     public_id
//                 }
//             }
//         }
//         aggregations(category: $category, subCategory: $subCategory, brands: $brands, colors: $colors, sizes: $sizes) {
//             count
//             colors {
//                 id
//                 key
//                 name
//             }
//             sizes
//             brands {
//                 slug
//                 description
//             }
//             categories {
//                 category {
//                     slug
//                     description
//                 }
//                 subCategories {
//                     slug
//                     description
//                 }
//             }
//         }
//     }
// `

function isNumeric(str) {
    if (typeof str != "string") return false // we only process strings!  
    return !isNaN(Number(str));
}

const ProductsContext = React.createContext({});

function persistContext(context) {
    localStorage.setItem('context', JSON.stringify(context));
}

function getContext() {
    if (typeof window === "undefined") return null;
    // window.localStorage
    const context = localStorage.getItem('context');
    if (!context) return null;

    try {
        return JSON.parse(context);
    } catch (error) {
        return null;
    }
}

export const ProductsProvider = props => {
    const { query } = useRouter();
    const router = useRouter();

    const first: string = query.first ? query.first.toString() : null;
    const second: string = query.second ? query.second.toString() : null;
    const third: number = query.page ? Number(query.page) : 1;
    console.log('url params', first, second, third);
    const [brands, setBrands] = useState([]);
    const [subCategories, setSubCategories] = useState([]);
    const [sizes, setSizes] = useState([]);
    const [colors, setColors] = useState([]);

    let secondIsPresent = false;
    let mode = '';
    let category: string = '';
    let subCategory: string = '';
    let brand: string = '';

    //let categories = first ? [first] : [];
    // let subCategoriesInit = [];
    let currentPage = 0;
    if (!second) {
        currentPage = 1;
    }
    else if (isNumeric(second)) {
        currentPage = Number(second);
    }
    else {
        //subCategoriesInit = [second];
        secondIsPresent = true;
        currentPage = third ? Number(third) : 1;
    }

    // let brandsInit = [];
    // let sizesInit = [];
    // let colorsInit = [];


    const context = getContext();

    const equals = (a, b) => a.length === b.length && a.every((v, i) => v === b[i]);

    const initializeFromContext = () => {
        // brandsInit = context.brands;
        // sizesInit = context.sizes;
        // colorsInit = context.colors;
        // subCategoriesInit = context.subCategories;
        setBrands(context.brands);
        setColors(context.colors);
        setSizes(context.sizes);
        if (!second) setSubCategories(context.subCategories);
    }

    useEffect(() => {
        console.log("secondIsPresent", secondIsPresent);
        if (context && secondIsPresent &&
            equals(context.categories, [first]) &&
            equals(context.subCategories, [second])) {
            console.log('context for category and subcategory');
            initializeFromContext();
        }
        else if (context && !secondIsPresent && equals(context.categories, [first])) {
            console.log('context for category');
            initializeFromContext();
        }
        else if (context && !secondIsPresent && equals(context.brands, [first])) {
            console.log('context for brands');
            initializeFromContext();
        }
        else {
            console.log("cleaning cintext");
            // brandsInit = [];
            // sizesInit = [];
            // colorsInit = [];
            // subCategoriesInit = [];

            setBrands([]);
            setSizes([]);
            setColors([]);
        }

    }, [router.asPath]);

    // const [getCategories, { loading: loadingCategories, data: allCategories }] = useLazyQuery(ALL_CATEGORIES_QUERY, {
    //     fetchPolicy: 'no-cache',
    // });
    // getCategories()

    //Load the aggregation general aggregation data, not the current query
    //so for example if the user is viewing a brand, we don't need aggregations for the categories
    //because we dont want to change the UI, to allow the user to change to another category but
    //within the same brand
    // const [getAggregations, { loading: loadingAggregations, data: dataAggregations }] = useLazyQuery(AGGREGATIONS_CATEGORY_QUERY, {
    //     variables: {
    //         category: mode === BRAND ? [] : categories,
    //         subCategory: mode === CATEGORY ? [] : subCategoriesInit.concat(subCategories),
    //         brands: mode === SUBCATEGORY ? [] : brandsInit.concat(brands),
    //     }
    // });

    // const [getProducts, { loading, data, refetch }] = useLazyQuery(ALL_PRODUCTS_QUERY, {
    //     variables: {
    //         category: categories,
    //         subCategory: subCategoriesInit.concat(subCategories),
    //         brands: brandsInit.concat(brands),
    //         sizes: sizesInit.concat(sizes),
    //         // colors: colorsInit.concat(colors),
    //         colors,
    //         skip: (currentPage - 1) * perPage,
    //         first: perPage
    //     },
    //     //fetchPolicy: "network-only"
    // });


    // const { loadingCategories, allCategories } = useCategories();
    // // console.log('allCategories', loadingCategories, allCategories);

    // if (!loadingCategories && allCategories) {
    //     const cat = allCategories.find(cat => cat.slug === categories[0]);
    //     if (cat) {
    //         category = categories[0];
    //         subCategory = subCategoriesInit.length > 0 ? subCategoriesInit[0] : null;
    //         brand = null;
    //         mode = subCategory ? SUBCATEGORY : CATEGORY;
    //     }
    //     else {
    //         mode = BRAND;
    //         brandsInit = [categories[0]];
    //         brand = brandsInit[0];
    //         category = null;
    //         subCategory = null;
    //         categories = [];
    //         subCategoriesInit = [];
    //     }
    //     console.log(cat, mode, brandsInit, brand, category);

    //     getAggregations();
    //     getProducts();
    // }



    // const queryParams: QueryParams = {
    //     category: categories,
    //     subCategory: subCategoriesInit.concat(subCategories),
    //     brands: brandsInit.concat(brands),
    //     sizes: sizesInit.concat(sizes),
    //     colors,
    // }

    const queryParams: QueryParams = {
        param: [first],
        subCategory: secondIsPresent && second ? [second] : subCategories,
        brands,
        sizes,
        colors,
        // subCategory: second ? subCategoriesInit.concat([second]) : subCategoriesInit.concat(subCategories),
        // brands: brandsInit.concat(brands),
        // sizes: sizesInit.concat(sizes),
        // colors: colorsInit.concat(colors),
    }
    console.log('queryParams', queryParams, currentPage);

    const { loading, data, refetch } = useQuery(ALL_PRODUCTS_QUERY, {
        variables: {
            where: queryParams,
            skip: (currentPage - 1) * perPage,
            first: perPage
        },
        //fetchPolicy: "network-only"
    });

    const products: any[] = data ? data.products : [];
    const aggregations: Aggregation = data ? data.aggregations : { count: 0, colors: [], size: [], brands: [] };
    const aggregationsCategory: any[] = data ? data.aggregationsCategory : { count: 0, colors: [], size: [], brands: [], sizes: [] };
    mode = data ? data.aggregationsCategory.mode : '';
    // console.log('MODE', mode);
    // console.log(aggregations);
    // console.log(aggregationsCategory);

    if (mode === CATEGORY) {
        category = first;
    }
    else if (mode === SUBCATEGORY) {
        category = first;
        subCategory = second;
    }
    else if (mode === BRAND) {
        brand = first;
    }

    // if (context && mode === CATEGORY &&
    //     equals(context.categories, categories)) {
    //     initializeFromContext();
    // }
    // else if (context && mode === SUBCATEGORY &&
    //     equals(context.categories, categories) &&
    //     equals(context.subCategories, subCategoriesInit.concat(subCategories))) {
    //     initializeFromContext();
    // }
    // else if (context && mode === BRAND &&
    //     equals(context.brands, brandsInit.concat(brands))) {
    //     initializeFromContext();
    // }


    // const aggregationsCategory: any[] = loadingAggregations || dataAggregations.aggregations;
    // const products: any[] = loading || data.products;
    // const aggregations: Aggregation = loading || data.aggregations;


    const getContextToPersist = () => {
        return {
            categories: mode === CATEGORY || mode === SUBCATEGORY ? [category] : [],
            // subCategories: subCategoriesInit.concat(subCategories).concat(subCategory),
            subCategories: mode === SUBCATEGORY ? [second] : subCategories,
            brands: mode === BRAND ? [first] : brands,
            colors,
            sizes,
            // subCategories: second ? subCategoriesInit.concat([second]) : subCategoriesInit.concat(subCategories),
            // brands: brandsInit.concat(brands),
            // colors,
            // sizes: sizesInit.concat(sizes),
            currentPage
        }
    }

    const setSelectedBrands = (slugs: string[]): void => {
        setBrands(slugs);

        persistContext({
            ...getContextToPersist(),
            brands: slugs,
        });
    }

    const setSelectedColors = (keys: string[]): void => {
        setColors(keys);

        persistContext({
            ...getContextToPersist(),
            colors: keys,
        });
    }

    const setSelectedSizes = (keys: string[]): void => {
        setSizes(keys);

        persistContext({
            ...getContextToPersist(),
            sizes: keys,
        });
    }

    const setSelectedSubCategories = (slugs: string[]): void => {
        setSubCategories(slugs);

        persistContext({
            ...getContextToPersist(),
            subCategories: slugs,
        });
    }


    const values = {
        mode,
        category,
        subCategory,
        brand,

        categories: [category],
        subCategories,
        brands,
        colors,
        sizes,
        // subCategories: subCategoriesInit.concat(subCategories),
        // brands: brandsInit.concat(brands),
        // colors: colorsInit.concat(colors),
        // sizes: sizesInit.concat(brands),
        currentPage,
        // allCategories,
        // categorySelected,
        // subCategorySelected,
        loading,
        // loadingCategories,
        // loadingAggregations,
        aggregations,
        products,
        aggregationsCategory,
        setSubCategories,
        setBrands,
        setColors,
        setSizes,

        setSelectedBrands,
        setSelectedColors,
        setSelectedSizes,
        setSelectedSubCategories,
    }

    // console.log(values);

    return (
        <ProductsContext.Provider value={values}>{props.children}</ProductsContext.Provider>
    );
};
export const ProductConsumer = ProductsContext.Consumer;
export default ProductsContext;

ProductsProvider.propTypes = {
    children: PropTypes.node.isRequired,
};