// const { webkit } = require('playwright');

const { chromium, webkit, firefox, devices } = require('playwright');
const browserName = process.env.BROWSER || 'webkit';
const expect = require('expect');
let browser;
let context;
let page;

jest.setTimeout(450000);

beforeAll(async () => {
    browser = await { chromium, webkit, firefox }[browserName].launch({
        headless: false,
        slowMo: 50,
    });
});
afterAll(async () => {
    await browser.close();
});
beforeEach(async () => {
    context = await browser.newContext({
        viewport: {
            width: 1500,
            height: 900,
        }
    });

    // page = await browser.newPage();
    page = await context.newPage();
});
afterEach(async () => {
    await page.close();
});

describe('single product page', () => {
    it('Load single product page and change viewport ', async () => {
        // await page.goto('https://openstore-frontend.herokuapp.com/', {

        await page.goto('http://localhost:3001/product/saint-laurent-leather-snakeskin-print-pump');
        // await page.goto('http://localhost:3001/product/saint-laurent-leather-snakeskin-print-pump', {
        //     waitUntil: 'networkidle0',
        //     timeout: 0
        // });

        await page.waitForSelector('#main-img', { state: 'attached' });
        await page.waitForSelector('#product-details', { state: 'attached' });

        const title = await page.$$('h1');
        const titleText = await page.innerText('h1' + ' >> a');
        expect(title.length).toBe(1)
        expect(titleText).toBe('Always Avantgarde');

        const imagesStrip1 = await page.$$eval('#image-strip-1 >> li', elems => elems.map((el, index) => {
            return index
        }));
        expect(imagesStrip1.length).toBe(4);

        const imagesStrip2 = await page.$$eval('#image-strip-2 >> div', elems => elems.map((el, index) => {
            return index
        }));
        expect(imagesStrip2.length).toBe(4);

        const h3title = await page.$$eval('#product-details >> h3', elems => elems.map(el => {
            return {
                title: el.textContent.trim()
            }
            // return {
            //   title: el.querySelector('.post-title').textContent.trim(),
            //   description: el.querySelector('.post-description').textContent.trim(),
            //   href: el.href,
            // }
        }));
        expect(h3title[0].title).toBe('Saint Laurent');

        const sizes = await page.$$eval('article >> div', elems => elems.map(el => {
            return {
                size: el.textContent.trim()
            }
        }));
        expect(sizes.length).toBe(8);
        for (let i = 0; i <= 7; i++) {
            expect(sizes[i].size).toBe((i + 5).toString());
        }

        const sizesSelected = await page.$$eval('article >> .selected', elems => elems.map(el => {
            return {
                size: el.textContent.trim()
            }
        }));
        expect(sizesSelected[0].size).toBe('5');

        const alsoLike = await page.$$eval('#also-like >> .product-strip-elem', elems => elems.map(el => {
            const style = getComputedStyle(el);
            return {
                title: el.textContent.trim(),
                img: el.querySelector('img').src,
                visible: style.display !== 'none'
            }
        }));
        expect(alsoLike.filter(product => product.visible).length).toBe(5);

        for (const product of alsoLike) {
            expect(product.img.length).toBeGreaterThan(10);
        }

        const imageStrip2IsVisible = await page.evaluate(() => {
            const element = document.querySelector('#image-strip-2-container');
            const style = getComputedStyle(element);

            return style.display === 'block';
        });
        expect(imageStrip2IsVisible).toBeTruthy();

        const imageStrip1IsVisible = await page.evaluate(() => {
            const element = document.querySelector('#image-strip-1-container');
            const style = getComputedStyle(element);
            return style.display === 'block';
        });
        expect(imageStrip1IsVisible).toBeFalsy();

        await page.setViewportSize({
            width: 1300,
            height: 900,
        });

        await page.waitForTimeout(1000);

        const imageStrip2IsVisibleAfter = await page.evaluate(() => {
            const element = document.querySelector('#image-strip-2-container');
            const style = getComputedStyle(element);

            return style.display === 'block';
        });
        expect(imageStrip2IsVisibleAfter).toBeFalsy();

        const imageStrip1IsVisibleAfter = await page.evaluate(() => {
            const element = document.querySelector('#image-strip-1-container');
            const style = getComputedStyle(element);
            return style.display === 'block';
        });
        expect(imageStrip1IsVisibleAfter).toBeTruthy();

        const alsoLikeAfter = await page.$$eval('#also-like >> .product-strip-elem', elems => elems.map(el => {
            const style = getComputedStyle(el);
            return {
                title: el.textContent.trim(),
                img: el.querySelector('img').src,
                style: style.display,
                visible: style.display !== 'none'
            }
        }));
        expect(alsoLikeAfter.filter(product => product.visible).length).toBe(4);

        // await page.screenshot({ path: `example.png` });
    });

    it('Load single product page with a mobile version ', async () => {
        const pixel2 = devices['Pixel 2'];

        const context = await browser.newContext({
            ...pixel2,
            userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1',
            viewport: {
                width: 600,
                height: 900,
            }
        });

        const page = await context.newPage();

        await page.goto('http://localhost:3001/product/saint-laurent-leather-snakeskin-print-pump', {
            waitUntil: 'load',
            timeout: 150000
        });

        await page.waitForSelector('#main-img', { state: 'attached' });
        await page.waitForSelector('#product-details', { state: 'attached' });

        await page.waitForSelector('.carousel-root', { state: 'attached' });

        const imageStrip2IsVisible = await page.evaluate(() => {
            const element = document.querySelector('#image-strip-2-container');
            const style = getComputedStyle(element);
            return style.display === 'block';
        });
        expect(imageStrip2IsVisible).toBeFalsy();

        const imageStrip1IsVisible = await page.evaluate(() => {
            const element = document.querySelector('#image-strip-1-container');
            const style = getComputedStyle(element);
            return style.display === 'block';
        });
        expect(imageStrip1IsVisible).toBeFalsy();

        const alsoLike = await page.$$eval('#also-like >> .product-strip-elem', elems => elems.map(el => {
            const style = getComputedStyle(el);
            return {
                // title: el.textContent.trim(),
                // img: el.querySelector('img').src,
                visible: style.display !== 'none'
            }
        }));
        expect(alsoLike.filter(product => product.visible).length).toBe(5);

        // await page.screenshot({ path: `example.png` });
    });
});

describe('add to cart', () => {
    it('Add Product to Cart', async () => {
        // await page.goto('https://openstore-frontend.herokuapp.com/', {

        await page.goto('http://localhost:3001/product/saint-laurent-leather-snakeskin-print-pump', {
            waitUntil: 'load'
        });

        await page.waitForSelector('#main-img', { state: 'attached' });
        await page.waitForSelector('#product-details', { state: 'attached' });

        await page.click('article div:nth-child(3)');

        const sizesSelected = await page.$$eval('article >> .selected', elems => elems.map(el => {
            return {
                size: el.textContent.trim()
            }
        }));
        expect(sizesSelected[0].size).toBe('7');

        await page.waitForTimeout(100);

        await page.click('#add-to-cart-button');

        await page.waitForTimeout(1500);

        const countText = await page.innerText('.count');
        expect(countText).toBe('1');

        await page.waitForTimeout(100);

        const element = await page.waitForSelector('#cart');
        const transform = await element.evaluate((el) => {
            return window.getComputedStyle(el).transform;
        });
        await page.click('.count');

        await page.waitForTimeout(100);

        const elementAfter = await page.waitForSelector('#cart');
        const transformAfter = await elementAfter.evaluate((el) => {
            return window.getComputedStyle(el).transform;
        });

        expect(transform).not.toBe(transformAfter);

        const cartItems = await page.$$eval('#cart >> .cart-item', elems => elems.map(el => {
            return {
                name: el.querySelector('.cart-item-details p:nth-child(2)').innerText,
                size: el.querySelector('.cart-item-details p:nth-child(3)').innerText,
            }
        }));

        const productNameCart = cartItems[0].name;
        const productSizeCart = cartItems[0].size;

        const productNameList = await page.$$eval('#product-details .product-name', elems => elems.map(el => {
            return el.textContent.trim();
        }));
        const productName = productNameList[0];

        expect(productName).toBe(productNameCart);
        expect(productSizeCart.includes(sizesSelected[0].size)).toBeTruthy();

        // await page.waitForTimeout(1000);
    });
});

export { }


        // // Click on the cookie policy acceptance button if it exists
        // if ((await page.QuerySelectorAsync("#truste-consent-button")) != null)
        // {
        //     await page.ClickAsync("#truste-consent-button");
        // }

        // // Hover on Developers
        // await page.HoverAsync("li[aria-labelledby='nav-item-Developers']");

        // // Click on Quickstarts
        // await page.ClickAsync("a[href='https://auth0.com/docs/quickstarts/']");

        // // Hover on Regular Web App
        // await page.HoverAsync("div[data-type='webapp']");

        // // Click on Launch Quickstart
        // await page.ClickAsync("div[data-type='webapp'] button.btn-success");

        // // Search for ASP.NET Core
        // await page.TypeAsync("div.quickstart-search-input input", "ASP.NET Core");

        // // Click on ASP.NET Core logo
        // await page.ClickAsync("div[data-name='aspnet-core']");