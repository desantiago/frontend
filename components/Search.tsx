import styled, { keyframes } from 'styled-components';
import { useCombobox } from 'downshift';
import { gql, useLazyQuery } from '@apollo/client';
import lodash from 'lodash';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Image } from 'cloudinary-react';
import { cloudName } from '../config';

const SEARCH_PRODUCTS_QUERY = gql`
    query SEARCH_PRODUCTS_QUERY($term: String!) {
        searchTerms: search(term: $term) {
            key,
            name
            brand {
                description
            }
            thumbnail {
                image {
                    public_id
                }
            }                
        }
    }
`

export default function Search() {
    const router = useRouter();
    const [search, { loading, data }] = useLazyQuery(SEARCH_PRODUCTS_QUERY, {
        fetchPolicy: 'no-cache',
    });
    const items = data?.searchTerms || [];
    // console.log(items);
    const findItemsButChill = lodash.debounce(search, 350);

    const { isOpen, getMenuProps, getInputProps, getComboboxProps, getItemProps, highlightedIndex } = useCombobox({
        items,
        onInputValueChange({ inputValue }) {
            // console.log('input changed', inputValue);
            if (inputValue.length < 3) return;
            findItemsButChill({
                variables: {
                    term: inputValue
                }
            });
        },
        onSelectedItemChange({ selectedItem }) {
            router.push({
                pathname: `/product/${selectedItem.key}`,
            });
        },
        itemToString: (item: any) => item?.name || '',
    })

    return (
        <SearchStyles>
            <div {...getComboboxProps()}>
                <input {...getInputProps({
                    type: 'search',
                    placeholder: '',
                    id: 'search',
                    className: loading ? 'loading' : null,
                })} />
            </div>
            <DropDown {...getMenuProps()}>
                {
                    isOpen && items.map((item, index) => {
                        return (
                            <DropDownItem
                                key={item.key}
                                {...getItemProps({ item })}
                                highlighted={index === highlightedIndex}
                            >
                                {/* <img src={"http://127.0.0.1:8080/" + item.images[0].images[0].filename} /> */}
                                <Image
                                    cloudName={cloudName}
                                    publicId={item.thumbnail.image.public_id}
                                    width="70"
                                    crop="scale" />
                                <Link href={`/product/${item.key}`}>
                                    <a>
                                        <Brand>{item.brand.description}</Brand>
                                        <Name>{item.name}</Name>
                                    </a>
                                </Link>
                            </DropDownItem>
                        )
                    })
                }
                {isOpen && !items.length && !loading && (
                    <DropDownItem>Sorry, No items found </DropDownItem>
                )}
            </DropDown>
        </SearchStyles>
    )
}

const Name = styled.span`
    font-family: 'Inter', sans-serif;
    font-size: 1.0rem;
`
const Brand = styled.p`
    font-family: 'Barlow Condensed', sans-serif;
    font-size: 1.1rem;
`
const DropDown = styled.div`
    position: absolute;
    width: 100%;
    z-index: 2;
    border-right: 1px solid #eaeaea;
    border-left: 1px solid #eaeaea;
`;
// background: ${(props) => (props.highlighted ? '#f7f7f7' : 'white')};

const DropDownItem = styled.div`
    background-color: white;
    border-bottom: 1px solid #eaeaea;
    padding: 1rem;
    transition: all 0.2s;
    ${(props) => (props.highlighted ? 'padding-left: 2rem;' : null)};
    display: flex;
    align-items: center;
    border-left: 10px solid
    ${(props) => (props.highlighted ? props.theme.lightgrey : 'white')};
    img {
        margin-right: 10px;
        width: 70px;
    }

    a {
        color: inherit;
        text-decoration: none;
    }
`;

const glow = keyframes`
    from {
        box-shadow: 0 0 0px yellow;
    }
    to {
        box-shadow: 0 0 10px 1px yellow;
    }
`;

const SearchStyles = styled.div`
    position: relative;
    input {
        width: 100%;
        height: 28px;
        padding: 1px;
        border: 0;
        font-size: 1.1rem;
        border-right: 1px solid #eaeaea;
        &.loading {
            animation: ${glow} 0.5s ease-in-out infinite alternate;
        }

        &:focus{
            outline: none;
        }        
    }
`;
