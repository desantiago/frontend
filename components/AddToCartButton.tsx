import { useContext, useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import styled from 'styled-components';

import { useCart } from '../lib/cartContext';
import { useUser, CURRENT_USER_QUERY } from './User';
// import { authContext } from '../lib/auth';
import { useAuth } from '../lib/auth';

const ADD_TO_CART_MUTATION = gql`
    mutation ADD_TO_CART_MUTATION($quantity: Int!, $color: ID!, $size: String!, $productKey: ID!) {
        addCartItem(quantity: $quantity, color: $color, size: $size, productKey: $productKey) {
            key
        }
    }
`

export default function AddToCartButton({ productKey }) {
    const { colorSelected, sizeSelected } = useCart();
    const user = useUser();
    const {
        signUpTemp
    }: any = useAuth();

    const [addToCart, { loading }] = useMutation(ADD_TO_CART_MUTATION, {
        variables: {
            quantity: 1,
            color: colorSelected,
            size: sizeSelected,
            productKey,
        },
        refetchQueries: [{ query: CURRENT_USER_QUERY }]
    });

    const addToCartOp = async () => {
        console.log("addToCart ", {
            quantity: 1,
            color: colorSelected,
            size: sizeSelected,
            productKey,
        });

        if (!user) {
            // console.log('user is not logged, login as a temporal user');
            const token = await signUpTemp();
            // console.log('token for temporal user', token);
        }

        // console.log('now try to add the product to the cart');
        await addToCart();
    }

    const onAddToCart = async (e) => {
        e.preventDefault();
        await addToCartOp();
    }

    return (
        <AddButton id="add-to-cart-button" type="button" disabled={loading} onClick={(e) => onAddToCart(e)}>
            <i className="lni lni-briefcase"></i> &nbsp; Add To Cart
        </AddButton>
    )
}

const AddButton = styled.button`
    margin-top: 1.0rem;
    background-color: #494949;
    color: white;
    font-family: Arial, Helvetica, sans-serif;
    font-weight: bold;
    padding: 0.25rem 2.0rem 0.25rem 2.0rem;
    font-family: 'Inter', sans-serif;
`
