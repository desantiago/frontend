import styled from 'styled-components';

const FooterCont = styled.footer`
background-color: #202020;
color: #f1f1f1;
padding-top: 0.25rem;
padding-bottom: 1.5rem;
`
//.footer-column-logo
const FooterLogo = styled.p`
text-align: center;
font-family: 'Josefin Slab', serif;
font-size: 1.5rem;
color: rgb(93, 93, 93);

@media only screen and (min-width: 800px) {
  font-size: 3.0rem;
}
`
//.footer-container-wide
const FooterContainerWide = styled.div`
display: none;

@media only screen and (min-width: 800px) {
  margin-top: 1.0rem;
  display: flex;
  flex-direction: row;
  font-family: 'Inter', sans-serif;
}
`
//.footer-container-mobile
const FooterContainerMobile = styled.div`
color: #eaeaea;
display: block;
margin-top: 1.0rem;
@media only screen and (min-width: 800px) {
  display:none;
}
`
//.footer-column-left-empty
const FooterColLeftEmpty = styled.div`
width: 200px;
`
//.footer-column-right-empty
const FooterColRightEmpty = styled.div`
width: 200px;
`
//.footer-column-left
const FooterColLeft = styled.div`
width: 25%;
`
//.footer-column-center 
const FooterColCenter = styled.div`
width: 25%;
`
//.footer-column-right 
const FooterColRight = styled.div`
width: 25%;
`
// .footer-column-logo
const FooterColLogo = styled.div`
@media only screen and (min-width: 800px) {
    width: 25%;
}
`
//.footer-container-wide h3
const FooterTitle = styled.h3`
color: rgb(93, 93, 93);
margin-bottom: 0.5rem;
`
//.footer-container-wide ul
const FooterList = styled.ul`
list-style: none;
font-size: 0.8rem;
`

export default function Footer() {
    return (
        <FooterCont>
            <FooterContainerWide>
                <FooterColLeftEmpty />
                <FooterColLeft>
                    <FooterTitle>ABOUT US</FooterTitle>
                    <FooterList>
                        <li>Who we are</li>
                        <li>Carrers</li>
                    </FooterList>
                </FooterColLeft>
                <FooterColCenter>
                    <FooterTitle>SUPPORT</FooterTitle>
                    <FooterList>
                        <li>Contact Us</li>
                        <li>Help & FAQ</li>
                        <li>Shipping Policy</li>
                        <li>Returns & Exchanges</li>
                        <li>Order Status & Tracking</li>
                        <li>International Shipping</li>
                    </FooterList>
                </FooterColCenter>
                <FooterColRight>
                    <FooterTitle>BROWSE</FooterTitle>
                    <FooterList>
                        <li>Brands</li>
                        <li>New Arrivals</li>
                        <li>Shoes</li>
                        <li>Handbags</li>
                    </FooterList>
                </FooterColRight>
                <FooterColLogo>
                    <FooterLogo>ALWAYS Avantgarde</FooterLogo>
                </FooterColLogo>
                <FooterColRightEmpty />
            </FooterContainerWide>

            <FooterContainerMobile>
                <FooterColLogo>
                    <FooterLogo>ALWAYS Avantgarde <br /> 2021 Copyright</FooterLogo>
                </FooterColLogo>
            </FooterContainerMobile>
        </FooterCont>
    )
}
