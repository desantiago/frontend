import styled from 'styled-components';

// .products
export const Products = styled.div`
  margin-bottom: 2.0rem;
  max-width: 100vw;

  @media only screen and (min-width: 800px) {
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
    margin-bottom: 2.0rem;
    max-width: none;
  }
`
// .product-place-holder
export const PlaceHolder = styled.div`
  color: #494949;
  padding: 0px;
`
// .product-data
export const ProductData = styled.div`
  text-align: center;
`
export const ProductImg = styled.img`
  width: 100%;
`
// .brand
export const ProductBrand = styled.div`
  font-family: 'Barlow Condensed', sans-serif;
  font-size: 2.0rem;
`
// .name
export const ProductName = styled.div`
  font-family: 'Inter', sans-serif;
  font-size: 1.0rem;
`
// .price
export const ProductPrice = styled.div`
  font-family: 'Mada', sans-serif;
  font-size: 1.5rem;
  margin-top: 0.5rem;
`
// .product-place-holder
export const PlaceHolderPH = styled.div`
  color: #494949;
  padding: 0.5rem;
  paading-left: 2.0rem;
  padding-right: 2.0rem;
`

export const ProductImgPH = styled.div`
    width:100%;
    height: 350px;
    background-color: #f1f1f1;
`
export const ProductDataPH = styled.div`
  text-align: center;
  margin-bottom: 1.0rem;
`
export const ProductBrandPH = styled.div`
  font-family: 'Barlow Condensed', sans-serif;
  font-size: 2.0rem;
  margin-top: 0.5rem;
  background-color: #fafafa;
`
export const ProductNamePH = styled.div`
  font-family: 'Inter', sans-serif;
  font-size: 1.0rem;
  margin-top: 0.5rem;
  background-color: #fafafa;
`
export const ProductPricePH = styled.div`
  font-family: 'Mada', sans-serif;
  font-size: 1.5rem;
  margin-top: 0.5rem;
  background-color: #fafafa;
`
