import { useState } from 'react';
import styled from 'styled-components';
import { FilterContainer, CategoryOptions, Li, Filter, FilterList, FilterTitle, CheckBox, Input, Text } from './Filters';

//.filter-menu
const FilterMenuCont = styled.div`
    position: fixed;
    top: 0;
    font-weight: 100;
    background: #f8f8f8;
    width: 250px;
    height: 100%;
    z-index: 100;
    -webkit-box-shadow: -3px 0px 5px 0px rgba(0, 0, 0, 0.2);
    box-shadow: -3px 0px 5px 0px rgba(0, 0, 0, 0.2);
    right: -260px;

    transition: all 0.3s;
    -webkit-transition: all 0.36;

    &.filter-menu-open {
        transform: translate3d(-260px, 0, 0);
        animation-timing-function: 1s ease-in;    
    }
`
// .filter-menu-open
// const FilterMenuOpen = styled.div`
//     transform: translate3d(-260px, 0, 0);
//     animation-timing-function: 1s ease-in;
// `

//.filter-menu-container
const FilterMenuContainer = styled.div`
    overflow: auto;
    max-height: 98%;
    font-family: 'Inter', sans-serif;
    padding: 1.0rem;
`

//.filter-menu-container .primary
const LinkPrimary = styled.a`
    background-color: #494949;
    color: white;
    padding: 0.25rem 0.5rem 0.25rem 0.5rem;
    text-decoration: none;
    margin-right: 1.0rem;
`

//.filter-menu-container .secondary 
const LinkSecondary = styled.a`
    font-weight: bold;
    text-decoration: none;
    color: inherit;
`

//.filter-menu-container .divider 
const Divider = styled.div`
    height: 1.0rem;
`

//.options-mobile
const OptionsMobile = styled.div`
  margin-left: 2.0rem;
  margin-bottom: 1.0rem;
  margin-top: 0.5rem;

  @media only screen and (min-width: 800px)   {
    display: none;
  }
`

//.options-mobile a 
const OptionsMobileLink = styled.a`
  font-family: Arial, Helvetica, sans-serif;
  border: 1px solid #494949;
  color: #494949;
  padding: 0.25rem 0.5rem 0.25rem 0.5rem;
  text-decoration: none;
  margin-right: 1.0rem;
`


export default function FilterMenu() {

    const [filterMenuOpen, setFilterMenuOpen] = useState(false);

    const filterOpen = () => {
        setFilterMenuOpen(!filterMenuOpen);
    }

    return (
        <>
            <FilterMenuCont className={`${filterMenuOpen ? 'filter-menu-open' : ''}`}>
                <FilterMenuContainer>
                    <LinkPrimary href="#" onClick={filterOpen}>Done</LinkPrimary>
                    <LinkSecondary href="#">Clear All</LinkSecondary>

                    <Divider />

                    <FilterContainer>
                        <FilterTitle>Designers</FilterTitle>
                        <FilterList>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Alexandre Birman</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Alexander McQueen</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Amina Muadi</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Aquazzura</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>By Far</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Francesco Russo</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Jimmy Choo</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Manolo Blahnik</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Valentino Garavani</Text>
                                </CheckBox>
                            </Li>
                        </FilterList>
                    </FilterContainer>
                    <FilterContainer>
                        <FilterTitle>Size</FilterTitle>
                        <FilterList>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>5</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>5.5</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>6</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>6.5</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>7</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>8</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>9</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>10</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>11</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>12</Text>
                                </CheckBox>
                            </Li>
                        </FilterList>
                    </FilterContainer>
                    <FilterContainer>
                        <FilterTitle>Color</FilterTitle>
                        <FilterList>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Black</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Blush</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Orange</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Pink</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>Red</Text>
                                </CheckBox>
                            </Li>
                            <Li>
                                <CheckBox>
                                    <Input type="checkbox" />
                                    <Text>White</Text>
                                </CheckBox>
                            </Li>
                        </FilterList>
                    </FilterContainer>
                </FilterMenuContainer>
            </FilterMenuCont>

            <OptionsMobile>
                <OptionsMobileLink href="#" onClick={filterOpen}>Filters</OptionsMobileLink>
            </OptionsMobile>

        </>
    )
}