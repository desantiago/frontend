import { gql, useMutation } from '@apollo/client';
import styled from 'styled-components';
import { CURRENT_USER_QUERY } from './User';

export const REMOVE_FROM_CART_MUTATION = gql`
    mutation REMOVE_FROM_CART_MUTATION($key: String!) {
        removeCartItem(key: $key) {
            key
        }
    }
`

// function update(cache, payload) {
//     console.log(cache.identify(payload.data.removeCartItem));
//     console.log(cache.identify(payload.data.removeCartItem.key));
//     // cache.evict({ id: cache.identify(payload.data.removeCartItem.key) });
//     cache.evict({ id: payload.data.removeCartItem.key });
//     cache.gc();
// }

export default function RemoveFromCartButton({ cartItemKey }) {
    // console.log('key ---', cartItemKey);
    const [removeFromCart, { loading }] = useMutation(REMOVE_FROM_CART_MUTATION, {
        variables: {
            key: cartItemKey
        },
        //update
        refetchQueries: [{ query: CURRENT_USER_QUERY }]
    });

    const onRemove = () => {
        removeFromCart();
    }

    return (
        <RemoveButton data-testid={cartItemKey} disabled={loading} onClick={onRemove}><i className="lni lni-cross-circle"></i></RemoveButton>
    )
}

const RemoveButton = styled.div`
    cursor: pointer;
    font-size: 2.0rem;
    margin-right: 1.0rem;
    margin-top: 1.0rem;
`
