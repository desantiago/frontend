import Link from 'next/link'
import Nav from './Nav'
import Cart from './Cart'

import styled from 'styled-components';
import Menu from './styles/Menu';

const HeaderStyles = styled.h1`
    text-align: center;
    font-family: Josefin Slab, serif;
    font-size: 2.5rem;
    padding: 0.5rem;
    width: 100vw;
    a {
        color: var(--main-font-color);
        text-decoration: none;
    }
`

export default function Header() {
    return (
        <header >
            <HeaderStyles>
                <Link href='/'>
                    Always Avantgarde
                </Link>
            </HeaderStyles>
            <Menu />
            <Nav />
            <Cart />
        </header>
    )
}