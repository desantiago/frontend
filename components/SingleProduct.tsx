import { useEffect, useState } from 'react'
import styled from 'styled-components';
import ReactMarkdown from "react-markdown";
import { Image } from 'cloudinary-react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
var Carousel = require('react-responsive-carousel').Carousel;

import { File, Sizes, Color, Price, ImageFiles, Product, Images, Image as TypeImage, RelatedProduct, Category, SubCategory } from '../lib/types';
import { emptyColor, emptyFile, emptySizes, emptyProduct } from '../lib/emptyObjects';
import AddToCartButton from './AddToCartButton';
import { useCart } from '../lib/cartContext';
import formatMoney from '../lib/formatMoney';
import { cloudName } from '../config';
import ProductStrip from './ProductStrip';
import { PRODUCT_QUERY } from '../lib/queries';
import { gql, useQuery } from '@apollo/client';

// var MobileDetect = require('mobile-detect');
import MobileDetect from 'mobile-detect';
import loadConfig from 'next/dist/next-server/server/config';
const userAgent = typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent
let md = null;
if (userAgent) {
    md = new MobileDetect(userAgent);
    // console.log(md.mobile());          // 'Sony'
    // console.log(md.phone());           // 'Sony'
    // console.log(md.tablet());
}

interface SingleProductProps {
    id: string;
    // product: Product;
}

// const SingleProduct: React.FC<SingleProductProps> = ({ id }: SingleProductProps) => {

interface CategoryComponentProps {
    loading: boolean;
    category: Category
    subCategory: SubCategory
}

const CategoryComponent: React.FC<CategoryComponentProps> = ({ loading, category, subCategory }: CategoryComponentProps) => {
    if (loading) {
        return (
            <CategoryProductPH></CategoryProductPH>
        )
    }

    return (
        <CategoryProduct>
            {category.description} / {subCategory.description}
        </CategoryProduct>
    )
}

interface ImagesComponentProps {
    loading: boolean;
    images: ImageFiles[];
    onClickImage?: Function;
}
const ImagesOneComponent: React.FC<ImagesComponentProps> = ({ loading, images }: ImagesComponentProps) => {

    if (loading) return (<ImageList>{[0, 1, 2, 3].map((n) => <ImagePH key={'n' + n} />)} </ImageList>)

    return (
        <ImageList id="image-strip-1">
            {
                images.map(image => {
                    return (
                        <li key={image.file.id}>
                            {/* <img src={"http://127.0.0.1:8080/" + image.filename} /> */}
                            <Image
                                cloudName={cloudName}
                                publicId={image.file.public_id}
                                width="300"
                                crop="scale" />
                        </li>
                    )
                })
            }
        </ImageList>
    )
}

const ImagesTwoComponent: React.FC<ImagesComponentProps> = ({ loading, images, onClickImage }: ImagesComponentProps) => {

    if (loading) {
        return (
            <ImageContainer>
                {[0, 1, 2, 3].map((n) => <ImagePH key={'n' + n} />)}
            </ImageContainer>
        )
    }

    return (
        <ImageContainer id="image-strip-2">
            {
                images.map(image => {
                    return (
                        <ImageContainerDiv key={image.file.id} onClick={() => onClickImage(image)}>
                            {/* <img src={"http://127.0.0.1:8080/" + image.filename} /> */}
                            <Image
                                cloudName={cloudName}
                                publicId={image.file.public_id}
                                width="300"
                                crop="scale" />
                        </ImageContainerDiv>
                    )
                })
            }
        </ImageContainer>
    )
}

interface MainImageComponentProps {
    loading: boolean;
    images: ImageFiles[];
    imageSel: ImageFiles;
}
const MainImageComponent: React.FC<MainImageComponentProps> = ({ loading, imageSel, images }: MainImageComponentProps) => {
    if (loading) {
        return (
            <MainImg>
                <MainImagePH />
            </MainImg>
        )
    }

    return (
        <MainImg id="main-img">
            {md && !md.mobile() && imageSel && imageSel.file && <Image
                cloudName={cloudName}
                publicId={imageSel.file.public_id}
                width="1500"
                crop="scale" />}

            {md && md.mobile() && <Carousel showArrows={false} autoPlay={false} showStatus={false} showIndicators={false} showThumbs={false}>
                {
                    images.map(image => {
                        return (
                            <SliderImg key={image.file.public_id}>
                                <Image
                                    cloudName={cloudName}
                                    publicId={image.file.public_id}
                                    width="800"
                                    crop="scale" />
                            </SliderImg>
                        )
                    })
                }
            </Carousel>}
        </MainImg>
    )
}

interface DetailsComponentProps {
    loading: boolean;
    id: string;
    product: Product;
    price: number;
    sizes: Sizes;
    sizeSel: string;
    onClickSize: Function;
}
const DetailsComponent: React.FC<DetailsComponentProps> = ({ loading, id, product, price, sizes, sizeSel, onClickSize }: DetailsComponentProps) => {
    if (loading) {
        return (
            <ProductDetails>
                <TitlePH>&nbsp;</TitlePH>
                <NamePH>&nbsp;</NamePH>
                <PriceSelPH>&nbsp;</PriceSelPH>

                <SizeOptions>
                    {[0, 1, 2, 3, 4, 5].map((idx) => <SizeOptionPH key={`size${idx}`} />)}
                </SizeOptions>

                <AddToCartButton productKey={id}></AddToCartButton>

                <DetailsTitle>Details</DetailsTitle>
                <DescriptionPH>
                </DescriptionPH>
            </ProductDetails>
        )
    }

    return (
        <ProductDetails id="product-details">
            <Title>{product.brand.description}</Title>
            <Name className="product-name">{product.name}</Name>
            <PriceSel>{formatMoney(price)}</PriceSel>

            <SizeOptions>
                {
                    sizes.sizes.map(size => {
                        return (
                            <SizeOption
                                key={`size${size}`}
                                className={size === sizeSel ? 'selected' : ''}
                                onClick={() => onClickSize(size)}
                            >
                                {size}
                            </SizeOption>
                        )
                    })
                }
            </SizeOptions>

            <AddToCartButton productKey={product.id}></AddToCartButton>

            <DetailsTitle>Details</DetailsTitle>
            <Description>
                <Markdown source={product.description} />
                <Markdown source={product.details} />
                <br /><br />
                <Markdown source={product.sizeAndFit} />
            </Description>
        </ProductDetails>
    )
}

const SingleProduct: React.FC<SingleProductProps> = ({ id }: SingleProductProps) => {
    const { setColorSelected, setSizeSelected } = useCart();

    const [color, setColor] = useState<Color>(emptyColor);
    const [images, setImages] = useState<ImageFiles[]>([]);
    const [imageSel, setImageSel] = useState<ImageFiles>({ file: emptyFile, order: 0 });
    const [sizes, setSizes] = useState<Sizes>(emptySizes);
    const [sizeSel, setSizeSel] = useState<string>('');
    const [price, setPrice] = useState(0);
    const [relatedProducts, setRelatedProducts] = useState([]);

    const [product, setProduct] = useState(emptyProduct);

    const { loading, data, refetch } = useQuery(PRODUCT_QUERY, {
        variables: {
            id: id
        },
        //fetchPolicy: "network-only"
    });

    const getPrice = (product, color, size): Price => {
        return product.
            prices.find(prices => prices.color.id === color).
            prices.find(price => price.size === size);
    }

    useEffect(() => {
        if (!data || !data.product) return;
        console.log(data.product);
        const firstColor: Color = data.product.colors[0];
        let imagesColor: ImageFiles[] = data.product.images
            .find((images: Images) => images.color.id === firstColor.id)
            .images.slice()
            .sort((a: TypeImage, b: TypeImage) => a.order - b.order);
        // console.log(imagesColor);
        // console.log(imagesColor.sort((a: TypeImage, b: TypeImage) => a.order - b.order));

        const sizesColor: Sizes = data.product.sizes.find((sizes: Sizes) => sizes.color.id === firstColor.id)

        setProduct(data.product);
        setColor(firstColor);
        setImages(imagesColor);
        setImageSel(imagesColor[0]);
        setSizes(sizesColor);
        setSizeSel(sizesColor.sizes[0]);

        setColorSelected(firstColor.id);
        setSizeSelected(sizesColor.sizes[0]);

        setPrice(getPrice(data.product, firstColor.id, sizesColor.sizes[0]).price);

        const relatedProducts = [...data.product.relatedProducts]
            .sort((a: RelatedProduct, b: RelatedProduct) => a.order - b.order)
            .map((related: RelatedProduct) => related.product);
        setRelatedProducts(relatedProducts);
    }, [data]);

    useEffect(() => {
        setImageSel({ file: emptyFile, order: 0 });
    }, [id]);

    const onClickSize = (size: string) => {
        setSizeSel(size);
        setSizeSelected(size);
        setPrice(getPrice(product, color.id, sizeSel).price)
    }

    const onClickImage = (image: TypeImage) => {
        setImageSel(image)
    }

    return (
        <main>
            <CategoryComponent loading={loading} category={product.category} subCategory={product.subCategory} />
            <Container>
                <ImageStrip>
                    <ImageStripOne id="image-strip-1-container">
                        <ImagesOneComponent loading={loading} images={images} />
                    </ImageStripOne>
                    <ImageStripTwo id="image-strip-2-container">
                        <ImagesTwoComponent loading={loading} images={images} onClickImage={onClickImage} />
                    </ImageStripTwo>
                </ImageStrip>
                <MainImageComponent loading={loading} imageSel={imageSel} images={images} />
                <div>
                    <DetailsComponent
                        loading={loading}
                        id={id}
                        product={product}
                        price={price}
                        sizes={sizes}
                        sizeSel={sizeSel}
                        onClickSize={onClickSize} />
                </div>
            </Container>
            <AlsoLike id="also-like">
                <AlsoLikeTitle>You may also like</AlsoLikeTitle>
                {!loading && <ProductStrip products={relatedProducts} />}
            </AlsoLike>
        </main>
    );
}

export default SingleProduct;

const Markdown = styled(ReactMarkdown)`
    ul {
        padding-left: 1.0rem;
    }
`

const SliderImg = styled.div`
    img {
        width: 100%;
        height: auto;
    }
`

const CategoryProduct = styled.div`
    padding-left: 2.0rem; 
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 0.9rem; 
    font-weight: bold; 
    padding-top: 0.25rem; 
    padding-bottom: 0.25rem;
    text-transform: uppercase;
`

const Container = styled.div`
    @media only screen and (min-width: 800px)   {
        display: grid;
        grid-gap: 0px;
        grid-template-columns: 150px 1fr 1fr;
    }

    @media only screen and (min-width: 1400px)   {
        display: grid;
        grid-gap: 0px;
        grid-template-columns: 400px 1fr 1fr;
    }
`

const ImageStrip = styled.div`
    max-height: 600px;
    overflow: hidden;
    text-align: center;

    @media only screen and (min-width: 1400px) {
        max-height: 800px;
        overflow: hidden;
        text-align: center;
    }
`

const ImageStripOne = styled.div`
    display: none;

    @media only screen and (min-width: 800px) {
        display: block;
    }

    @media only screen and (min-width: 1400px) {
        display: none
    }
`
const ImageStripTwo = styled.div`
    display: none;

    @media only screen and (min-width: 1400px) {
        padding-left: 2.0rem;
        display: block;
        padding-right: 20px;
    }
`
// const ButtonUp = styled.div`
//     padding-left: 2.0rem;
//     height: 45px;
// `
// const ButtonDown = styled.div`
//     padding-left: 2.0rem;
//     height: 45px;
//     margin-top: 15px;
// `
const ImageList = styled.ul`
    list-style: none;
    max-height: 510px;
    overflow: hidden;
    padding-left: 2.0rem;

    img {
        width: 100px;
        height: auto;
    }
`
const ImageContainer = styled.div`
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
`
const ImageContainerDiv = styled.div`
    margin-bottom: 0.5rem;
    cursor: pointer;
    img {
        width: 160px;
        height: auto;
    }
`
const MainImg = styled.div`
    text-align: center;

    img {
        width: 100%;
        max-width: 600px;
    }
`

const ProductDetails = styled.div`
    margin-left: 2.0rem;
    margin-right: 2.0rem;
`
const Title = styled.h3`
    margin-top: 1.0rem;
    font-family: 'Barlow Condensed', sans-serif;
    font-size: 2.0rem;
`
const Name = styled.p`
    font-family: 'Inter', sans-serif;
    font-size: 1.0rem;
`
const PriceSel = styled.p`
    font-family: 'Mada', sans-serif;
    font-size: 1.5rem;
    margin-top: 0.5rem;
`
const Description = styled.div`
    font-size: 0.9rem;
    margin-top: 2.0rem;
    font-family: 'Comfortaa', sans-serif;
`
const SizeOptions = styled.article`
    margin-top: 0.5rem;
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(50px, 1fr));
    max-width: none;
`
const SizeOption = styled.div`
    width: 40px;
    height: 40px;
    background-color: #efefef;
    color: #494949;
    font-weight: bold;
    line-height: 40px;
    font-family: 'Inter', sans-serif;
    font-size: 1.25rem;
    text-align: center;
    margin-top: 1.0rem;
    cursor: pointer;

    &.selected {
        background-color:#EF3B68;
        color: #fff;
    }
`

const DetailsTitle = styled.h4`
    margin-top: 2.0rem;
    font-size: 1.0rem;
    font-family: 'M PLUS 1p', sans-serif;
    border-bottom: 1px solid #eaeaea;
    text-transform: uppercase;
`
const AlsoLike = styled.section`
    margin-top: 2.0rem;
    padding-left: 2.0rem;
    padding-right: 2.0rem;
    margin-bottom: 2.0rem;
`
const AlsoLikeTitle = styled.h4`
    font-size: 1.2rem;
    font-family: 'M PLUS 1p', sans-serif;
    border-bottom: 1px solid #eaeaea;
    text-transform: uppercase;
`


const CategoryProductPH = styled.div`
    padding-left: 2.0rem; 
    font-size: 0.9rem; 
    font-weight: bold; 
    padding-top: 0.25rem; 
    padding-bottom: 0.25rem;
`

const ImagePH = styled.div`
    width: 100px;
    height: 150px;
    background-color: #f1f1f1;
    margin-bottom: 1.0rem;
`

const TitlePH = styled.h3`
    margin-top: 1.0rem;
    font-size: 2.0rem;
    background-color: #f1f1f1;
`
const NamePH = styled.p`
    font-size: 1.0rem;
    background-color: #f1f1f1;
`
const PriceSelPH = styled.p`
    margin-top: 0.5rem;
    background-color: #f1f1f1;
`
const DescriptionPH = styled.div`
    font-size: 0.9rem;
    margin-top: 2.0rem;
    background-color: #f1f1f1;
    height: 80px;
`
const SizeOptionPH = styled.div`
    width: 40px;
    height: 40px;
    background-color: #f1f1f1;
`
const MainImagePH = styled.div`
    width: 100%;
    height: 600px;
    background-color: #f1f1f1;
`
