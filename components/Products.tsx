import { useContext } from 'react';
import Head from 'next/head'
import styled from 'styled-components';

import {
    Products,
    ProductData,
    PlaceHolder,
    ProductImg,
    ProductBrand,
    ProductName,
    ProductPrice,
    PlaceHolderPH,
    ProductBrandPH,
    ProductDataPH,
    ProductImgPH,
    ProductNamePH,
    ProductPricePH
} from './styles/Product';

import FilterMenu from './styles/FilterMenu';
import SidebarFilters from './SidebarFilters';
import Link from 'next/link';
import PaginationLinks from './PaginationLinks';
import ProductsContext from '../lib/productsContext';
import formatMoney from '../lib/formatMoney';
import Filters from './Filters';
import { Image } from 'cloudinary-react';
import { cloudName } from '../config';

export default function ProductsContainer() {
    const {
        loading,
        loadingCategories,
        aggregations,
        products,
        category,
        subCategory,
        currentPage,
        brand,
    }: any = useContext(ProductsContext);

    if (loadingCategories) {
        console.log('loadingCategories...');
        return <div></div>
    }
    // if (loading) {
    //     console.log('loading....');
    //     return <div></div>
    // }
    {/* <ProductImg src={"http://127.0.0.1:8080/" + product.images[0].images[0].filename} /> */ }

    return (
        <MainWraper>
            <SideBar>
                <SidebarFilters />
            </SideBar>

            <Content>
                <Filters />
                <FilterMenu />

                <Products>
                    {
                        loading && [0, 1, 2, 3, 4, 5, 6, 7].map((n) => {
                            return (
                                <PlaceHolderPH key={'ph' + n}>
                                    <ProductImgPH>
                                    </ProductImgPH>

                                    <ProductDataPH>
                                        <ProductBrandPH>&nbsp;</ProductBrandPH>
                                        <ProductNamePH>&nbsp;</ProductNamePH>
                                        <ProductPricePH>&nbsp;</ProductPricePH>
                                    </ProductDataPH>
                                </PlaceHolderPH>
                            )
                        })
                    }

                    {!loading && Array.isArray(products) && products.map(product => {
                        return (
                            <div key={product.id + "link"}>
                                <Link href={"/product/" + product.slug}>
                                    <a style={{ textDecoration: 'none' }}>
                                        <PlaceHolder key={product.id}>
                                            <ScaleImage>
                                                <Image
                                                    cloudName={cloudName}
                                                    publicId={product.mainImage.image.public_id}
                                                    width="500"
                                                    crop="scale" />
                                            </ScaleImage>

                                            <ProductData>
                                                <ProductBrand>{product.brand.description}</ProductBrand>
                                                <ProductName>{product.name}</ProductName>
                                                <ProductPrice>{formatMoney(product.prices[0].prices[0].price)}</ProductPrice>
                                            </ProductData>
                                        </PlaceHolder>
                                    </a>
                                </Link>
                            </div>
                        )
                    })}
                </Products>
                {!loading && <PaginationLinks currentPage={currentPage} category={category} subCategory={subCategory} brand={brand} numProducts={aggregations.count} />}
            </Content>
        </MainWraper>
    )
}


const MainWraper = styled.div`
  display:block;

  @media only screen and (min-width: 800px)   {
    display: grid;
    grid-gap: 0px;
    grid-template-columns: 200px auto;
    grid-template-areas:
    "sidebar content"
  }
`
const SideBar = styled.aside`
  display:none;
  @media only screen and (min-width: 800px)   {
    display: block;
    grid-area: sidebar;
  }
`;

const Content = styled.main`
  grid-area: content;
`

const ScaleImage = styled.div`
    img {
        width: 100%
    }
`