import Link from 'next/link';
import React, { useState, useContext, useEffect, Fragment } from 'react';

import { useCategories } from '../lib/categoriesProvider';
import ProductsContext from '../lib/productsContext';
import {
    FilterContainer,
    CategoryOptions,
    Li,
    Filter,
    FilterList,
    FilterTitle,
    CheckBox,
    Input,
    Text,
    FilterPH,
    FilterListPH,
    FilterTitlePH,
    LiPH
} from './styles/Filters';

import { CATEGORY, SUBCATEGORY, BRAND } from '../lib/types';

export default function SidebarFilters() {
    const {
        mode,
        category,
        subCategory,
        subCategories,
        brands,
        colors,
        sizes,
        aggregations,
        aggregationsCategory,
        loadingAggregations,
        loading,

        setSelectedBrands,
        setSelectedColors,
        setSelectedSizes,
        setSelectedSubCategories,
    }: any = useContext(ProductsContext);

    const {
        allCategories,
        loadingCategories,
    } = useCategories();

    const getSubCategories = () => {
        if (mode !== SUBCATEGORY && !allCategories.length) return null;
        const categoryData = allCategories.find(cat => cat.slug === category);
        if (categoryData) {
            return categoryData.subcategories
        }
        return [];
    }

    const subCategoriesData = getSubCategories();

    const { brands: brandsAgg, colors: colorsAgg, sizes: sizesAgg, categories: categoriesAgg } = aggregationsCategory;
    const { brands: brandsCurrentAgg, colors: colorsCurrentAgg, sizes: sizesCurrentAgg } = aggregations;

    // console.log('SidebarFilters', subCategories);
    // console.log(subCategory);
    // console.log("categoriesAgg", categoriesAgg);
    // console.log(colorsAgg);
    // console.log(sizesAgg);
    // console.log(colorsCurrentAgg);
    // console.log(sizesCurrentAgg);

    const catsInit = () => {
        return categoriesAgg ? categoriesAgg.map(cat => {
            return {
                ...cat,
                subCategories: cat.subCategories.map(subCat => {
                    return {
                        ...subCat,
                        selected: subCategories.includes(subCat.slug)
                    }
                }).sort((a, b) => a.description.localeCompare(b.description))
            }
        }).sort((a, b) => a.category.description.localeCompare(b.category.description)) : [];
    }
    useEffect(() => {
        setCategoryData(catsInit());
    }, [categoriesAgg]);
    const [categoryData, setCategoryData] = useState(catsInit());

    const brandsInit = () => {
        return brandsAgg ? brandsAgg.map(brand => {
            return {
                ...brand,
                selected: brands.includes(brand.slug)
            }
        }).sort((a, b) => a.description.localeCompare(b.description)) : [];
    }
    useEffect(() => {
        setBrandData(brandsInit());
    }, [brands, brandsCurrentAgg]);
    const [brandData, setBrandData] = useState(brandsInit());


    const colorsInit = () => {
        return colorsAgg ? colorsAgg.map(color => {
            return {
                ...color,
                selected: colors.includes(color.id),
                enabled: colorsCurrentAgg.some(agg => agg.id === color.id)
            }
        }).sort((a, b) => a.name.localeCompare(b.name)) : [];
    }
    useEffect(() => {
        setColorData(colorsInit());
    }, [colors, colorsCurrentAgg]);
    const [colorData, setColorData] = useState(colorsInit());


    const sizesInit = () => {
        return sizesAgg ? sizesAgg.map(size => {
            return {
                size,
                selected: sizes.includes(size),
                enabled: sizesCurrentAgg.some(agg => agg === size)
            }
        }).sort((a, b) => Number(a.size) - Number(b.size)) : [];
    }
    useEffect(() => {
        setSizeData(sizesInit());
    }, [sizes, sizesCurrentAgg]);
    const [sizeData, setSizeData] = useState(sizesInit());

    const handleCategoryChange = (changeEvent: React.ChangeEvent<HTMLInputElement>): void => {
        const { name } = changeEvent.target;

        categoryData.forEach(cat => {
            cat.subCategories.forEach(subCat => {
                if (subCat.slug === name) {
                    subCat.selected = !subCat.selected
                }
            });
        });

        const catSel = categoryData.map(cat => {
            return cat.subCategories.filter(subCat => subCat.selected).map(subCat => subCat.slug)
        }).flat();
        // console.log("catSel", catSel);
        setSelectedSubCategories(catSel);
    };

    const handleBrandChange = (changeEvent: React.ChangeEvent<HTMLInputElement>): void => {
        const { name } = changeEvent.target;

        brandData.forEach(element => {
            if (element.slug === name) {
                element.selected = !element.selected
            }
        });
        setSelectedBrands(brandData.filter(brand => brand.selected).map(brand => brand.slug));
    };

    const handleSizesChange = (changeEvent: React.ChangeEvent<HTMLInputElement>): void => {
        const { name } = changeEvent.target;
        sizeData.forEach(element => {
            if (element.size === name) {
                element.selected = !element.selected
            }
        });
        setSelectedSizes(sizeData.filter(size => size.selected).map(size => size.size));
    };

    const handleColorChange = (changeEvent: React.ChangeEvent<HTMLInputElement>): void => {
        const { name } = changeEvent.target;
        colorData.forEach(element => {
            if (element.id === name) {
                element.selected = !element.selected
            }
        });
        setSelectedColors(colorData.filter(color => color.selected).map(color => color.id));
    };

    if (loadingCategories) return <div></div>
    if (loadingAggregations) return <div></div>

    return (
        <FilterContainer>

            {
                loading && [0, 1].map((n) => {
                    return (
                        <FilterPH key={'ph' + n}>
                            <FilterTitlePH>&nbsp;</FilterTitlePH>
                            <FilterListPH>
                                <LiPH>&nbsp;</LiPH>
                                <LiPH>&nbsp;</LiPH>
                                <LiPH>&nbsp;</LiPH>
                                <LiPH>&nbsp;</LiPH>
                            </FilterListPH>
                        </FilterPH>
                    )
                })
            }

            { !loading && <Filter>
                <CategoryOptions>
                    {
                        (mode === SUBCATEGORY) && subCategoriesData.map((subCat) => {
                            if (subCat.slug === subCategory) {
                                return (
                                    <strong key={subCat.key}>
                                        <li>
                                            {subCat.description}
                                        </li>
                                    </strong>
                                )
                            }
                            else {
                                return (
                                    <li key={subCat.key}>
                                        <Link href={`/products/${category}/${subCat.slug}`}>
                                            <a style={{ textDecoration: 'none', color: 'inherit' }}>
                                                {subCat.description}
                                            </a>
                                        </Link>
                                    </li>
                                )
                            }
                        })
                    }
                    {
                        (mode === BRAND || mode === CATEGORY) && categoryData.map(cat => {
                            return (
                                <Fragment key={cat.category.slug}>
                                    <Li key={cat.category.slug + 'sub'}><strong>{cat.category.description}</strong></Li>
                                    {
                                        cat.subCategories.map(subCat => {
                                            return (
                                                <Li key={subCat.slug}>
                                                    <CheckBox>
                                                        <Input
                                                            type="checkbox"
                                                            name={subCat.slug}
                                                            checked={subCat.selected}
                                                            onChange={handleCategoryChange}
                                                        />
                                                        <Text>{subCat.description}</Text>
                                                    </CheckBox>
                                                </Li>
                                            )
                                        })
                                    }
                                </Fragment>
                            )
                        })
                    }
                </CategoryOptions>
            </Filter>}
            {
                !loading && mode === SUBCATEGORY && <Filter>
                    <FilterTitle>Designers</FilterTitle>
                    <FilterList key="brandlist">
                        {
                            brandData.map(brand => {
                                return (
                                    <Li key={brand.slug}>
                                        <CheckBox>
                                            <Input
                                                type="checkbox"
                                                name={brand.slug}
                                                checked={brand.selected}
                                                onChange={handleBrandChange}
                                            />
                                            <Text>{brand.description}</Text>
                                        </CheckBox>
                                    </Li>
                                )
                            })
                        }
                    </FilterList>
                </Filter>
            }
            { !loading && <Filter>
                <FilterTitle>Size</FilterTitle>
                <FilterList key="sizeslist">
                    {
                        sizeData.map(size => {
                            return (
                                <Li key={"key" + size.size}>
                                    <CheckBox className={size.enabled ? null : 'disabled'}>
                                        <Input
                                            type="checkbox"
                                            name={size.size}
                                            checked={size.selected}
                                            onChange={handleSizesChange}
                                        />
                                        <Text>{size.size}</Text>
                                    </CheckBox>
                                </Li>
                            )
                        })
                    }
                </FilterList>
            </Filter>
            }
            { !loading && <Filter>
                <FilterTitle>Color</FilterTitle>
                <FilterList key="colorlist">
                    {
                        colorData.map(color => {
                            return (
                                <Li key={color.id}>
                                    <CheckBox className={color.enabled ? null : 'disabled'}>
                                        <Input
                                            type="checkbox"
                                            name={color.id}
                                            checked={color.selected}
                                            disabled={!color.enabled}
                                            onChange={handleColorChange}
                                        />
                                        <Text>{color.name}</Text>
                                    </CheckBox>
                                </Li>
                            )
                        })
                    }
                </FilterList>
            </Filter>}

        </FilterContainer>
    )
}
