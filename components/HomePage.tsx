import { gql, useLazyQuery, useQuery } from '@apollo/client';
import styled from 'styled-components';
import Link from 'next/link';

import ProductStrip from './ProductStrip';

const NEW_ARRIVALS_QUERY = gql`
    query NEW_ARRIVALS_QUERY($subCategory: String) {
        productsByParams(where: { category: [$subCategory], sortBy: "timeStamp", sortDirection: "DESC"}, skip: 0, first: 6) {
            id
            name
            slug
            brand {
                description
            }
            prices {
                prices {
                    size
                    price
                }
            }        
            mainImage {
                image {
                    public_id
                }
            }
        }
    }
`

export default function HomePage() {

    const { loading: loadingShoes, data: dataShoes, refetch: refetchShoes } = useQuery(NEW_ARRIVALS_QUERY, {
        variables: {
            subCategory: 'shoes'
        },
    });

    const { loading: loadingBags, data: dataBags, refetch: refetchBags } = useQuery(NEW_ARRIVALS_QUERY, {
        variables: {
            subCategory: 'bags'
        },
    });

    return (
        <Wrapper>
            <div className="hero">
                <img className="heroimg" src="/resources/hero.webp" />
            </div>
            <div className="content">
                <h2>Shop Shoe Arrivals</h2>
                {!loadingShoes && dataShoes && <ProductStrip products={dataShoes.productsByParams} />}

                <h2>Shop Bags Arrivals</h2>
                {!loadingBags && dataBags && <ProductStrip products={dataBags.productsByParams} />}

                <br />
                <h2>Shop By Shoe Category</h2>
                <CategoriesStrip>
                    <Link href={"products/shoes/pumps"}>
                        <a>
                            <Category>
                                <img src="/resources/category-pumps.webp" />
                                <h3>Pumps</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/shoes/sandals"}>
                        <a>
                            <Category>
                                <img src="/resources/category-sandals.webp" />
                                <h3>Sandals</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/shoes/flats"}>
                        <a>
                            <Category>
                                <img src="/resources/category-flats.jpg" />
                                <h3>Flats</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/shoes/boots"}>
                        <a>
                            <Category>
                                <img src="/resources/category-boots.webp" />
                                <h3>Boots & Booties</h3>
                            </Category>
                        </a>
                    </Link>
                </CategoriesStrip>

                {/* <h2>Shop By Category</h2>
                <CategoriesStrip>
                    <Category>
                        <img src="/resources/category-clutches-2.webp" />
                        <h3>Clutches</h3>
                    </Category>
                    <Category>
                        <img src="/resources/category-crossbody-2.jpg" />
                        <h3>Cross Body</h3>
                    </Category>
                    <Category>
                        <img src="/resources/category-mini-2.webp" />
                        <h3>Mini Bags</h3>
                    </Category>
                    <Category>
                        <img src="/resources/category-shoulder-2.webp" />
                        <h3>Shoulder Bags</h3>
                    </Category>
                    <Category>
                        <img src="/resources/category-tote-2.webp" />
                        <h3>Tote Bags</h3>
                    </Category>
                </CategoriesStrip> */}

                <br />
                <h2>Shop By Bag Category</h2>
                <CategoriesStrip>
                    <Link href={"products/bags/clutches"}>
                        <a>
                            <Category>
                                <img src="/resources/category-clutches.jpg" />
                                <h3>Clutches</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/bags/crossbody"}>
                        <a>
                            <Category>
                                <img src="/resources/category-crossbody.jpg" />
                                <h3>Cross Body</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/bags/minibags"}>
                        <a>
                            <Category>
                                <img src="/resources/category-mini.jpg" />
                                <h3>Mini Bags</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/bags/shoulder"}>
                        <a>
                            <Category>
                                <img src="/resources/category-shoulder.webp" />
                                <h3>Shoulder Bags</h3>
                            </Category>
                        </a>
                    </Link>
                    <Link href={"products/bags/totes"}>
                        <a>
                            <Category>
                                <img src="/resources/category-tote.webp" />
                                <h3>Tote Bags</h3>
                            </Category>
                        </a>
                    </Link>
                </CategoriesStrip>

            </div>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    margin-top: 1.0rem;

    .hero {
        margin: 0;
    }
    .content {
        margin: auto 1.0rem;
    }
    img {
        width: 100%;
    }

    .content {
        margin-top: 2.0rem;
    }

    @media only screen and (max-width: 800px) {
        .heroimg {
            width: 100%; 
            height: 500px; 
            object-fit: cover; 
            object-position: 50% 0;
        }     
    }

    @media only screen and (min-width: 801px) and (max-width: 1200px) {
        .heroimg {
            width: 100%; 
            height: 600px; 
            object-fit: cover; 
            object-position: 50% 0;
        }     
    }

    @media only screen and (min-width: 1426px) {
        .hero {
            margin: auto 5.0rem;
        }
    }

    @media only screen and (min-width: 1426px) {
        .content {
            margin: auto 5.0rem;
            margin-top: 2.0rem;
        }
    }

    h2 {
        font-family: 'Mada';
        font-size: 2.0rem;
    }
`

const CategoriesStrip = styled.div`
    margin-top: 1.0rem;
    margin-bottom: 1.0rem;
    display: grid;
    grid-gap: 1.0rem;
    max-width: none;
    text-align: center;

    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));

    a {
        text-decoration: none;
        color: inherit;
    }

    @media only screen and (min-width: 200px) and (max-width: 865px) {
        grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
    }

    @media only screen and (min-width: 866px) and (max-width: 1000px) {
        grid-template-columns: repeat(auto-fit, minmax(400px, 1fr));
    }
`

const Category = styled.div`
    img {
        width: 100%; 
    }

    h3 {
        font-family: 'Inter';
        font-size: 1.25rem;
    }
`