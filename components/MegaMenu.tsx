import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import styled from 'styled-components';

export default function MegaMenu() {
    const router = useRouter();

    const [menuVisible, setMenuVisible] = useState({
        shoes: false,
        bags: false,
    })

    useEffect(() => {
        setMenuVisible({ bags: false, shoes: false })
    }, [router.asPath]);

    return (
        <NavReact>
            <ul>
                <li>
                    <OptionMenu
                        type="button"
                        className={menuVisible.shoes ? "overc" : ''}
                        onMouseEnter={() => setMenuVisible({ bags: false, shoes: true })}
                        onMouseLeave={() => setMenuVisible({ bags: false, shoes: false })}
                    >
                        Shoes
                    </OptionMenu>
                    <MenuOption
                        className={menuVisible.shoes ? "open" : ''}
                        onMouseEnter={() => setMenuVisible({ bags: false, shoes: true })}
                        onMouseLeave={() => setMenuVisible({ bags: false, shoes: false })}
                    >
                        <img style={{ width: '300px', height: 'auto' }} src="/resources/jimmychoo.webp" />
                        <MenuOptions>
                            <h3>By Type</h3>
                            <Link href="/products/shoes/boots">Boots & Booties</Link>
                            <Link href="/products/shoes/flats">Flats</Link>
                            <Link href="/products/shoes/pumps">Pumps</Link>
                            <Link href="/products/shoes/sandals">Sandals</Link>
                            {/* <a href="#">Sades</a>
                        <a href="#">Sangbacks</a>
                        <a href="#">Sneakers</a>
                        <a href="#">Wedges</a> */}
                        </MenuOptions>
                        <MenuOptions>
                            <h3>By Designer</h3>
                            <Link href="/products/byfar">By Far</Link>
                            <Link href="/products/francescorusso">Francesco Russo</Link>
                            <Link href="/products/jimmychoo">Jimmy Choo</Link>
                            <Link href="/products/manoloblahnik">Manolo Blahnik</Link>
                            <Link href="/products/valentinogaravani">Valentino Garavani</Link>
                        </MenuOptions>
                    </MenuOption>
                </li>
                <li>
                    <OptionMenu
                        type="button"
                        className={menuVisible.bags ? "overc" : ''}
                        onMouseEnter={() => setMenuVisible({ shoes: false, bags: true })}
                        onMouseLeave={() => setMenuVisible({ shoes: false, bags: false })}
                    >
                        Bags
                </OptionMenu>
                    <MenuOption
                        className={menuVisible.bags ? "open" : ''}
                        onMouseEnter={() => setMenuVisible({ shoes: false, bags: true })}
                        onMouseLeave={() => setMenuVisible({ shoes: false, bags: false })}
                    >
                        <img style={{ width: '300px', height: 'auto' }} src="/resources/stella.webp" />
                        <MenuOptions>
                            <h3>By Type</h3>
                            {/* <a href="#">Backpacks</a>
                        <a href="#">Belt</a> */}
                            <Link href="/products/bags/clutches">Clutches</Link>
                            <Link href="/products/bags/crossbody">Cross Body</Link>
                            {/* <a href="#">Evening </a>
                        <a href="#">Luggage and Travel</a> */}
                            <Link href="/products/bags/minibags">Mini Bags</Link>
                            <Link href="/products/bags/shoulder">Shoulder</Link>
                            <Link href="/products/bags/totes">Totes</Link>
                        </MenuOptions>
                        <MenuOptions>
                            <h3>By Designer</h3>
                            <Link href="/products/alexandermcqueen">Alexander McQueen</Link>
                            <Link href="/products/balmain">Balmain</Link>
                            <Link href="/products/markcross">Mark Cross</Link>
                            <Link href="/products/marni">Marni</Link>
                            <Link href="/products/staud">Staud</Link>
                            <Link href="/products/isabelmarant">Isabel Marant</Link>
                            <Link href="/products/wandler">Wandler</Link>
                            <Link href="/products/proenzaschouler">Proenza Schouler</Link>
                        </MenuOptions>
                    </MenuOption>
                </li>
            </ul>
        </NavReact>

    )
}


const NavReact = styled.div`
    padding: 0 1rem;
    position: sticky;
    top: 0;
    display: grid;
    place-items: center;

    ul {
        grid-auto-flow: column;
        list-style: none;
        margin: 0;
        padding: 0;
        display: grid;
    
        li {
            padding: 0;
            margin: 0 0.5rem;
        }
    }
`;

// &:hover,
// &:focus {
//     border-top-color: var(--main-color);
//     color: #f1f1f1;
//     background-color: #202020;
// }

const OptionMenu = styled.button`
    border-radius: 0;
    padding-top: 0.1rem;
    background-color: white;
    padding-left: 1.0rem;
    padding-right: 1.0rem;
    border-top: 3px solid transparent;
    transition: 280ms all 120ms ease-out;
    font-family: 'Mada', sans-serif;
    font-size: 1.0rem;

    &.overc {
        border-top-color: var(--main-color);
        color: #f1f1f1;
        background-color: #202020;
    }
`;

const MenuOption = styled.div`
    position: absolute; 
    left: 50%;       
    top: calc(100% - 0.05rem);
    transition: 280ms all 120ms ease-out;
    transform: rotateX(-90deg) translateX(-50%);
    transform-origin: top center;
    visibility: hidden;
    opacity: 0.0;
    box-shadow: 0 0.15em 0.25em rgba(black, 0.25);
    z-index: 1000;

    &.open {
        opacity: 1;
        transform: rotateX(0) translateX(-50%);
        visibility: visible;
        a {
            opacity: 1;
        }
    }

    a {
        color: #f1f1f1;
        font-family: 'Inter', sans-serif;
        font-size: 0.9rem;
        display: block;
        padding-bottom: 0.5em;
        opacity: 0;
        transition: 280ms all 120ms ease-out;
        text-decoration: none;

        &:hover {
            color: #fff;
            font-weight: bold;
        }

        &:focus {
            outline: none;
            color: #fff;
            font-weight: bold;
        }
    }

    background-color: #202020;
    display: grid;
    grid-template-columns: 33fr 33fr 33fr;
    width: 700px;
    text-align: left;

    h3 {
        font-size: 1.0rem;
        font-weight: bold; 
        margin-bottom: 0.5rem;
        font-family: 'M PLUS 1p', sans-serif;
        text-transform: uppercase;
        color: rgb(93,93,93);
    }
    p {
        margin-bottom: 0.25rem;
    }
    img { 
        width: 100%;
        height: auto;
    }
`

const MenuOptions = styled.div`
    margin-left: 1.5rem;
    margin-top: 1.0rem;
`
