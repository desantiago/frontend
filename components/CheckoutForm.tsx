import { useState } from 'react';
import styled from 'styled-components';

import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import { StripeError } from '@stripe/stripe-js';
import nProgress from 'nprogress';
import { gql, useMutation } from '@apollo/client';

import { CURRENT_USER_QUERY } from './User';
import { useRouter } from 'next/router';

const CHECKOUT_MUTATION = gql`
    mutation CHECKOUT_MUTATION($stripeId: String!) {
        checkOut(stripeId: $stripeId) {
            id,
            total
            orderItems {
                key
                productName
            }
        }
    }
`

const FormStyles = styled.form`
    box-shadow: 0 1px 2px 2px rgba(0, 0, 0, 0.04);
    border: 1px solid rgba(0, 0, 0, 0.06);
    border-radius: 5px;
    padding: 1.0rem;
    width: 500px;
    margin: 1.0rem 2.0rem;
`

const Title = styled.h2`
    margin: 1.0rem 2.0rem;
    font-family: 'Inter', sans-serif;
`

const Button = styled.button`
    color: white;
    background-color: #EF3B68;
    font-weight: bold;
    padding: 0.25rem 2.0rem;
    font-size: 1.1rem;
    border-radius: 3px;
    margin-top: 1.0rem;
`

export default function CheckoutForm() {

    const [checkOut, { error: checkOutError }] = useMutation(CHECKOUT_MUTATION, {
        refetchQueries: [{ query: CURRENT_USER_QUERY }]
    });

    const [error, setError] = useState<StripeError>();
    const [loading, setLoading] = useState(false);
    const stripe = useStripe();
    const router = useRouter();
    const elements = useElements();

    async function handleSubmit(e) {
        e.preventDefault();

        setLoading(true);
        nProgress.start();

        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: 'card',
            card: elements.getElement(CardElement),
        });

        console.log(paymentMethod);

        if (error) {
            setError(error);
            nProgress.done();
        }

        const order = await checkOut({
            variables: {
                stripeId: paymentMethod.id,
            },
        });

        console.log(order);

        // router.push({
        //     pathname: `/finished`,
        // });

        setLoading(false);
        nProgress.done(0);
    }

    return (
        <>
            <Title>Checkout</Title>
            <FormStyles onSubmit={handleSubmit}>
                {error && <p style={{ fontSize: 12 }}>{error.message}</p>}

                <CardElement></CardElement>
                <Button type="submit">Make Payment</Button>
            </FormStyles>
        </>
    )
}