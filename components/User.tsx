import { gql, useQuery } from "@apollo/client";

export const CURRENT_USER_QUERY = gql`
    query {
        authenticatedUser {
            name
            email
            cartItems {
                id
                quantity
                size
                color {
                    name
                }
                price
                total
                product {
                    key
                    name
                    brand {
                        description
                    }
                    thumbnail {
                        image {
                            public_id
                        }
                    }
                    mainImage {
                        image {
                            public_id
                        }
                    }
                }
            }
        }
    }
`

export function useUser() {
    const { data } = useQuery(CURRENT_USER_QUERY);
    return data?.authenticatedUser;
}