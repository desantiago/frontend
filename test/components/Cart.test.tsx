import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { CURRENT_USER_QUERY } from '../../components/User';
import { useUser } from '../../components/User';
import Cart from '../../components/Cart';
import { act } from 'react-dom/test-utils';
import { REMOVE_FROM_CART_MUTATION } from '../../components/RemoveFromCartButton';

const user = {
    name: 'Test User',
    email: 'test@mail.com',
    cartItems: [
        {
            id: '424242-424242',
            quantity: 1,
            size: '7',
            color: {
                name: 'Black'
            },
            price: 55000,
            total: 55000,
            product: {
                key: '12121212-1212122',
                name: 'Product Name 1',
                brand: {
                    description: 'Brand Name'
                },
                thumbnail: {
                    image: {
                        public_id: 'Id'
                    }
                },
                mainImage: {
                    image: {
                        public_id: 'Id'
                    }
                }
            }
        },
        {
            id: '5454545-5454545',
            quantity: 1,
            size: '7',
            color: {
                name: 'Black'
            },
            price: 45000,
            total: 45000,
            product: {
                key: '1313131-13131313',
                name: 'Product Name 2',
                brand: {
                    description: 'Brand Name'
                },
                thumbnail: {
                    image: {
                        public_id: 'Id'
                    }
                },
                mainImage: {
                    image: {
                        public_id: 'Id'
                    }
                }
            }
        }
    ]
}

const userAfter = {
    name: 'Test User',
    email: 'test@mail.com',
    cartItems: [
        {
            id: '424242-424242',
            quantity: 1,
            size: '7',
            color: {
                name: 'Black'
            },
            price: 55000,
            total: 55000,
            product: {
                key: '12121212-1212122',
                name: 'Product Name 1',
                brand: {
                    description: 'Brand Name'
                },
                thumbnail: {
                    image: {
                        public_id: 'Id'
                    }
                },
                mainImage: {
                    image: {
                        public_id: 'Id'
                    }
                }
            }
        },
    ]
}

// jest.mock('../../components/User', () => ({
//     useUser: () => user
// }));

describe('Cart', () => {
    test('renders cart component', async () => {

        const mocks = [
            {
                request: {
                    query: CURRENT_USER_QUERY,
                    variables: {},
                },
                result: {
                    data: {
                        authenticatedUser: user,
                    },
                },
            },
            {
                request: {
                    query: REMOVE_FROM_CART_MUTATION,
                    variables: { key: '5454545-5454545' },
                },
                result: {
                    data: {
                        removeCartItem: {
                            key: '5454545-5454545'
                        },
                        // authenticatedUser: userAfter,
                    }
                },
            },
            {
                request: {
                    query: CURRENT_USER_QUERY,
                    variables: {},
                },
                result: {
                    data: {
                        authenticatedUser: userAfter,
                    },
                },
            },
        ];

        act(() => {
            render(
                <MockedProvider mocks={mocks} addTypename={false}>
                    <Cart />
                </MockedProvider>
            );
        });

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        //expect(screen.getByText('5')).toBeInTheDocument();
        expect(screen.getByText('Product Name 1')).toBeInTheDocument();
        expect(screen.getByText('Product Name 2')).toBeInTheDocument();
        expect(screen.getByText('Test User', { exact: false })).toBeInTheDocument();
        expect(screen.getByText('$1,000')).toBeInTheDocument();

        const removeButton = screen.getByTestId('5454545-5454545');
        act(() => {
            fireEvent.click(removeButton);
        });

        await act(async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
        });

        // screen.debug();

        expect(screen.getAllByText('$550').length).toBe(2);
        expect(screen.queryByText('Product Name 2')).not.toBeInTheDocument();
    });
});