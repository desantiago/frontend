import React from 'react';
import { render, screen } from '@testing-library/react';

import CartCount from '../../components/CartCount';

describe('CartCount', () => {
    test('renders CartCount component', () => {
        render(<CartCount count={5} />);
        expect(screen.getByText('5')).toBeInTheDocument();
    });
});