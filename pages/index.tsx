import HomePage from '../components/HomePage';


function Home() {
  return (
    <HomePage />
  )
}

// export const getStaticProps: GetStaticProps = async (context) => {
//   // console.log("context: ", context);

//   const client2 = new ApolloClient({
//     uri: 'http://localhost:3000/graphql',
//     cache: new InMemoryCache()
//   });

//   const { data } = await client2.query({
//     query: ALL_CATEGORIES_QUERY
//   });

//   // console.log("---------------------------------");
//   if (data.categories) {
//     const subCategory = data.categories[0].subcategories[0].key;
//     // console.log("1", subCategory);

//     const { data: dataProducts } = await client2.query({
//       query: ALL_PRODUCTS_QUERY,
//       variables: {
//         subCategory
//       }
//     });

//     return {
//       props: {
//         categories: data.categories,
//         productsServer: dataProducts.products,
//         aggregationsServer: dataProducts.aggregations
//       }
//     }
//   }
//   else {
//     return {
//       props: {
//         categories: data.categories,
//         productsServer: [],
//         aggregations: []
//       }
//     }
//   }
// }

export default Home;

// Trading one unit of stock
// =========================
//
// This is a hypothetical scenario, where we are given prices of a stock ahead
// of time. The prices vary daily, and we know all daily prices in the future,
// for the next N consecutive days.
//
// Input: daily prices of stock, from day 1 to day N inclusively
// Restriction: not more than one unit of stock on hand at any time
// You can make any number of buys and sells
// Goal: maximize profit
// Output: maximal profit
//
// Example: [ 10, 8, 11, 12, 9, 11, 10 ]
// Analysis:
//   If we were to buy on day 1 and sell on day 2 -- then we are loosing money
//   If we were to buy on day 2 and sell on day 4 -- gain profit +4
//   If we were to buy on day 5 and sell on day 6 -- gain profit +2
//   max profit possible for current input --> 4 + 2 = 6
// Expected output for this example: 6



// NEW PROBLEM
// The same as before
// Additional restriction: At most one buy and sell is allowed


// function getMaxProfit(input) {
//   let currentProfit = 0;
//   let totalProfit = 0;
//   let possibleProfit = [];

//   for (let i = 0; i < input.length - 1; i++) {
//     if ((input[i+1] - input[i]) > 0) {
//       // profit
//       currentProfit += input[i+1] - input[i];
//     }
//     else {
//       // loss, we need to sell
//       //totalProfit += currentProfit;
//       possibleProfit.push(currentProfit)
//       //currentProfit = 0;
//     }
//   }

//   // totalProfit += currentProfit;  
//   if (currentProfit > 0) {
//     possibleProfit.push(currentProfit);
//   }

//   console.log(possibleProfit);

//   let maxProfit = 0;
//   possibleProfit.forEach((profit) => {
//     if (profit > maxProfit) maxProfit = profit;
//   })

//   return maxProfit;
// }


// https://www.seattletimes.com/life/food-drink/here-are-the-5-best-sandwiches-our-food-critic-tan-vinh-has-had-in-seattle-this-month/