import { AppProps } from 'next/app'
import './_app.css'
import Page from '../components/Page'
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { ApolloProvider } from '@apollo/client';
import withData from '../lib/withData';
// import { AuthProvider } from '../lib/auth';
import { AuthStateProvider } from '../lib/auth';
import { CartStateProvider } from '../lib/cartContext';
import { createGlobalStyle } from 'styled-components'

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const GlobalStyles = createGlobalStyle`
html {
    --red: #ff0000;
    --black: #ff0000;
    --grey: #3A33A3;
    --main-font-color: #494949;
    --main-color: #EF3B68;
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing:inherit
}
body {
    padding:0;
    margin:0;
    overflow-x: hidden;
}
* {
    padding: 0;
    margin: 0;
    border: 0;
}
`

// function App({ Component, pageProps, apollo }) {
//   return (
//     <AuthProvider>
//       <CartStateProvider>
//         <Page>
//           <Component {...pageProps} />
//         </Page>
//       </CartStateProvider>
//     </AuthProvider>
//   )
// }

function App({ Component, pageProps, apollo }) {
  return (
    <AuthStateProvider>
      <CartStateProvider>
        <GlobalStyles />

        <Page>
          <Component {...pageProps} />
        </Page>
      </CartStateProvider>
    </AuthStateProvider>
  )
}


App.getInitialProps = async function ({ Component, ctx }) {
  let pageProps: any = {};
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }
  pageProps.query = ctx.query;
  return pageProps;
}

export default withData(App as any)