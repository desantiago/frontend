import { ApolloClient, gql, InMemoryCache } from '@apollo/client';
import { GetStaticPaths, GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import SingleProduct from '../../components/SingleProduct';

export const PRODUCT_QUERY = gql`
    query PRODUCT_QUERY($id: String) {
        product: productByIds(id: $id) {
            name
            description
            sizeAndFit
            details
            category {
                key
                description
            }
            subCategory {
                key
                description
            }
            brand {
                key
                description
            }
            colors {
                id
                key
                name
            }
            images {
                color {
                    id
                    key
                    name
                }
                images {
                    file {
                        id
                        filename
                        public_id
                    }
                    order
                }
            }
            sizes {
                color {
                    id
                    key
                    name
                }
                sizes
            }
            prices {
                color {
                    id
                    key
                    name
                }
                prices {
                    size
                    price
                }
            }
            relatedProducts {
                order
                product {
                    id
                    name
                    slug
                    brand {
                        description
                    }
                    prices {
                        prices {
                            size
                            price
                        }
                    }        
                    thumbnail {
                        image {
                            public_id
                        }
                    }
                    mainImage {
                        image {
                            public_id
                        }
                    }    
                }
            }
        }
    }
`

function ProductPage() {
    const router = useRouter()
    console.log('router query', router.query);
    const { id } = router.query

    const id2: string = Array.isArray(id) ? id[0] : id;
    // console.log('ProductPage ', id2);

    return (
        <SingleProduct id={id2} />
    )
}

// function ProductPage({ product }) {
//     const router = useRouter()
//     const { paramId } = router.query

//     const id: string = Array.isArray(paramId) ? paramId[0] : paramId;

//     return (
//         <SingleProduct id={id} product={product} />
//     )
// }

// export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {
// export const getStaticPaths: GetStaticPaths<{ id: string }> = async () => {
//     return {
//         paths: [], //indicates that no page needs be created at build time
//         fallback: 'blocking' //indicates the type of fallback
//     }
// }

// export const getStaticProps: GetStaticProps = async (context) => {
//     console.log("getStaticProps SingleProduct");
//     console.log("context: ", context);

//     // let isMobileView = (context.req
//     //     ? context.req.headers['user-agent']
//     //     : navigator.userAgent).match(
//     //       /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
//     //     )    

//     const client2 = new ApolloClient({
//         uri: 'http://localhost:3000/graphql',
//         cache: new InMemoryCache()
//     });

//     const { data } = await client2.query({
//         query: PRODUCT_QUERY,
//         variables: {
//             id: context.params.id
//         }
//     });

//     return {
//         props: {
//             product: data.product,
//         }
//     }
// }

export default ProductPage;