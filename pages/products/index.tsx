import Head from 'next/head'
import ProductsContainer from '../../components/Products';
import { CategoriesStateProvider } from '../../lib/categoriesProvider';
import { ProductsProvider } from '../../lib/productsContext';

function ProductPage() {
  return (
    <CategoriesStateProvider>
      <ProductsProvider>
        <Head>
          <title>Always Avantgarde</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <ProductsContainer />
      </ProductsProvider>
    </CategoriesStateProvider>
  )
}

export default ProductPage;
