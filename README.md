This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3001](http://localhost:3001) with your browser to see the result.


Frontend for the store, it's NextJS/React application execute it following the previous instructions.

Main Dependencies: 
- NextJS and React
- Apollo GraphQL Client
- Stripe - to handle the payments

Will update the config options later.

It's Work in progress is going to change a lot in the following commits.